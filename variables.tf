#
# Variables Configuration
#
variable "cluster_name" {
  type        = string
  description = "Name of the created EKS cluster"
  default     = "k8s-chaos-experimental"
}

variable "kubernetes_version" {
  type    = string
  default = "1.17"
}

variable "workers_count" {
  default = 3
}

variable "workers_type" {
  type    = string
  default = "t2.small"
}

variable "region" {
  type        = string
  description = "Name of the region to create EKS cluster in"
  default     = "eu-central-1"
}
