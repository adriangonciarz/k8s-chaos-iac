terraform {
  backend "remote" {
    organization = "agtest"
    workspaces {
      name = "k8s-chaos"
    }
  }
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = ">= 2.1.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.1.0"
    }
  }
}

provider "aws" {
  region = var.region
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

data "aws_availability_zones" "available" {
}

# Security Groups
resource "aws_security_group" "worker_group_mgmt_one" {
  name_prefix = "worker_group_mgmt_one"
  description = "AWS Security Group for EKS cluster"
  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8"
    ]
  }
  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group" "all_worker_mgmt" {
  name_prefix = "all_worker_mgmt"
  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
      "172.16.0.0/12",
      "192.168.0.0/16",
    ]
  }
  vpc_id = module.vpc.vpc_id
}

# VPC
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 2.47"

  name                 = "k8s-chaos-vpc"
  cidr                 = "10.0.0.0/16"
  azs                  = data.aws_availability_zones.available.names
  private_subnets      = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets       = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  public_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                    = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                    = "1"
  }
}


#EKS Cluster
module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "15.1.0"

  cluster_name                    = var.cluster_name
  cluster_version                 = var.kubernetes_version
  cluster_create_timeout          = "1h"
  cluster_endpoint_private_access = true

  vpc_id  = module.vpc.vpc_id
  subnets = module.vpc.private_subnets

  worker_groups = [
    {
      name                          = "worker-group-1"
      instance_type                 = var.workers_type
      asg_desired_capacity          = var.workers_count
      additional_security_group_ids = [aws_security_group.worker_group_mgmt_one.id]
    },
  ]
  workers_group_defaults = {
    root_volume_type = "gp2"
  }
  worker_additional_security_group_ids = [aws_security_group.all_worker_mgmt.id]
}

# Example K8S entities - namespace, deployment, service
provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

### Helm Deployments
provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
    exec {
      api_version = "client.authentication.k8s.io/v1alpha1"
      args        = ["eks", "get-token", "--cluster-name", var.cluster_name]
      command     = "aws"
    }
  }
}

# Sock shop deployment
resource "kubernetes_namespace" "sock_shop" {
  metadata {
    name = "sock-shop"
  }
}


# Litmus deployment
resource "kubernetes_namespace" "litmus" {
  metadata {
    name = "litmus"
  }
}

resource "helm_release" "litmus" {
  name       = "litmus"

  repository = "https://litmuschaos.github.io/litmus-helm/"
  chart      = "litmus"

  namespace  = kubernetes_namespace.litmus.metadata.0.name
}

resource "helm_release" "litmus_portal" {
  name       = "litmus-portal"

  repository = "https://litmuschaos.github.io/litmus-helm/"
  chart      = "litmus-portal"

  namespace  = kubernetes_namespace.litmus.metadata.0.name
}
